package com.example.cryptocurrency.home

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class HomeViewModel : ViewModel() {

    companion object {
        val bitcoinSymbol = "BTCEUR"
        val litecoinSymbol = "LTCEUR"
        val moneroSymbol = "XMREUR"
        val ethereumSymbol = "ETHEUR"
    }

    val navigation = MutableLiveData<Map<String, Int>>()

    fun navigate(to: Int, symbol: String){
        navigation.value = mapOf(symbol to to)
    }
}