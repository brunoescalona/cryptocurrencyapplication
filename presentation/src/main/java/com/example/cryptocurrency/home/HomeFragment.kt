package com.example.cryptocurrency.home

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.example.cryptocurrency.databinding.FragmentHomeBinding
import com.example.cryptocurrency.detail.DetailFragment

class HomeFragment : Fragment() {

    private lateinit var viewModel: HomeViewModel
    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()

        configureView(inflater, container)
        configureObservers()

        return binding.root
    }

    private fun configureView(inflater: LayoutInflater, container: ViewGroup?){
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        this.viewModel = HomeViewModel()
        binding.viewModel = viewModel
        binding.setLifecycleOwner(this)
    }

    private fun configureObservers(){
        viewModel.navigation.observe(this, Observer {
            val bundle = Bundle()
            it?.let {
                val symbol = it.keys.first()
                bundle.putString(DetailFragment.SYMBOL_BUNDLE, symbol)
                findNavController().navigate(it[symbol]!!, bundle)
            }
        })
    }
}
