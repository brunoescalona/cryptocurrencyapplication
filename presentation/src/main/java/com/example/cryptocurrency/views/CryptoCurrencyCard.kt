package com.example.cryptocurrency.views

import com.example.cryptocurrency.R
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.design.card.MaterialCardView
import android.util.AttributeSet
import android.view.LayoutInflater
import com.example.cryptocurrency.databinding.CryptoCurrencyCardBinding

class CryptoCurrencyCard @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : MaterialCardView(context, attrs, defStyleAttr) {

    private val binding: CryptoCurrencyCardBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.crypto_currency_card, this, true)

    init {
        context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.CryptoCurrencyCard,
                0, 0).apply {
            try {
                val colorBackground = getColor(R.styleable.CryptoCurrencyCard_crypto_background, 0)
                val colorTint = getColor(R.styleable.CryptoCurrencyCard_crypto_tint, 0)

                binding.cryptoCurrencyCard.setCardBackgroundColor(colorBackground)
                binding.cryptoCurrencyLogo.setImageDrawable(getDrawable(R.styleable.CryptoCurrencyCard_crypto_icon))
                binding.cryptoCurrencyLogo.setColorFilter(colorTint, android.graphics.PorterDuff.Mode.SRC_IN)

                binding.cryptoCurrencyTitle.text = getString(R.styleable.CryptoCurrencyCard_crypto_title)
                binding.cryptoCurrencySubtitle.text = getString(R.styleable.CryptoCurrencyCard_crypto_subtitle)
                binding.cryptoCurrencyTitle.setTextColor(colorTint)
                binding.cryptoCurrencySubtitle.setTextColor(colorTint)
            } finally {
                recycle()
            }
        }
    }

}