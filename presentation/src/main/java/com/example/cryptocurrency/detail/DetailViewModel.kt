package com.example.cryptocurrency.detail

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.view.View
import com.example.domain.usecases.GetTickerDataPerSymbolUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class DetailViewModel(getTickerDataPerSymbolUseCase: GetTickerDataPerSymbolUseCase, symbol: String): ViewModel() {

    val loading = MutableLiveData<Int>()

    private var subscription: Disposable

    val model = MutableLiveData<DetailModel>()
    val errorMessage = MutableLiveData<String>()

    init{
        model.value = DetailModel(
                "0 €",
                "0 €",
                "0.0% last hour",
                "0.0% last day",
                "0.0% last week",
                "0.0% last month",
                ""
        )
        loading.value = View.GONE
        subscription = getTickerDataPerSymbolUseCase.execute("global", symbol)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRequestStart() }
                .map{DetailModelMapper.mapDomainModelToViewModel(it)}
                .subscribe(
                        {onRequestSuccess(it)},
                        {onRequestError(it)}
                )
    }

    private fun onRequestStart(){
        loading.value = View.VISIBLE
    }

    private fun onRequestSuccess(model: DetailModel){
        this.model.value = model
        loading.value = View.GONE
    }

    private fun onRequestError(throwable: Throwable){
        errorMessage.value = throwable.localizedMessage
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}