package com.example.cryptocurrency.detail

import android.arch.lifecycle.Observer
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.cryptocurrency.R

import com.example.cryptocurrency.databinding.FragmentDetailBinding
import com.example.cryptocurrency.home.HomeViewModel
import com.example.domain.usecases.GetTickerDataPerSymbolUseCase
import org.koin.android.ext.android.inject

class DetailFragment : Fragment() {

    companion object {
        const val SYMBOL_BUNDLE: String = "symbol"
    }

    private lateinit var viewModel: DetailViewModel
    private lateinit var binding: FragmentDetailBinding

    private val getTickerDataPerSymbolUseCase : GetTickerDataPerSymbolUseCase by inject()

    private lateinit var symbol: String

    private lateinit var toolbarTitle: String
    private var backgroundColor: Int = 0
    private var textColor: Int = 0
    private var icon: Drawable? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        symbol = arguments?.getString(SYMBOL_BUNDLE, null)!!

        setConfigurationVariables()
        configureToolbar()
        configureView(inflater, container)
        configureObservers()

        return binding.root
    }

    private fun configureToolbar(){
        val toolbar = (activity as AppCompatActivity).supportActionBar
        toolbar?.show()
        toolbar?.setDisplayHomeAsUpEnabled(true)
        toolbar?.setDisplayShowHomeEnabled(true)
        toolbar?.title = toolbarTitle
    }

    private fun configureView(inflater: LayoutInflater, container: ViewGroup?){
        binding = FragmentDetailBinding.inflate(inflater, container, false)
        this.viewModel = DetailViewModel(getTickerDataPerSymbolUseCase, symbol)
        binding.viewModel = viewModel
        binding.setLifecycleOwner(this)

        binding.constraintLayout.setBackgroundColor(backgroundColor)
        binding.textViewAskTitle.setTextColor(textColor)
        binding.textViewBidTitle.setTextColor(textColor)
        binding.textViewChangesTitle.setTextColor(textColor)
        binding.textViewLastUpdatedTitle.setTextColor(textColor)
        binding.icon.setImageDrawable(icon)
        binding.icon.setColorFilter(textColor)
    }

    private fun configureObservers(){
        viewModel.errorMessage.observe(this, Observer { message ->
            if (message != null) Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
        })
    }

    private fun setConfigurationVariables(){
        when(symbol){
            HomeViewModel.bitcoinSymbol -> {
                toolbarTitle = getString(R.string.bitcoin_title)
                backgroundColor = ContextCompat.getColor(context!!, R.color.colorBitcoin)
                textColor = ContextCompat.getColor(context!!, R.color.colorBitcoinDark)
                icon = ContextCompat.getDrawable(context!!, R.drawable.ic_bitcoin)
            }
            HomeViewModel.litecoinSymbol -> {
                toolbarTitle = getString(R.string.litecoin_title)
                backgroundColor = ContextCompat.getColor(context!!, R.color.colorLitecoin)
                textColor = ContextCompat.getColor(context!!, R.color.colorLitecoinDark)
                icon = ContextCompat.getDrawable(context!!, R.drawable.ic_litecoin)
            }
            HomeViewModel.ethereumSymbol -> {
                toolbarTitle = getString(R.string.ethereum_title)
                backgroundColor = ContextCompat.getColor(context!!, R.color.colorEthereum)
                textColor = ContextCompat.getColor(context!!, R.color.colorEthereumDark)
                icon = ContextCompat.getDrawable(context!!, R.drawable.ic_ethereum)
            }
            else -> {
                toolbarTitle = getString(R.string.monero_title)
                backgroundColor = ContextCompat.getColor(context!!, R.color.colorMonero)
                textColor = ContextCompat.getColor(context!!, R.color.colorMoneroDark)
                icon = ContextCompat.getDrawable(context!!, R.drawable.ic_monero)
            }
        }
    }
}
