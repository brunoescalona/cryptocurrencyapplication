package com.example.cryptocurrency.detail

import com.example.domain.model.CryptoCurrency
import java.lang.Exception

data class DetailModel(
        var ask: String,
        var bid: String,
        var changesHour: String,
        var changesDay: String,
        var changesWeek: String,
        var changesMonth: String,
        var updated: String
)

object DetailModelMapper {
    fun mapDomainModelToViewModel(cryptoCurrency: CryptoCurrency): DetailModel {
        return DetailModel("${twoDecimals(cryptoCurrency.ask)} €",
                "${twoDecimals(cryptoCurrency.bid)} €",
                "${twoDecimals(cryptoCurrency.changesHour)}% last hour",
                "${twoDecimals(cryptoCurrency.changesDay)}% last day",
                "${twoDecimals(cryptoCurrency.changesWeek)}% last week",
                "${twoDecimals(cryptoCurrency.changesMonth)}% last month",
                "Last updated ${cryptoCurrency.updated}"
        )
    }

    private fun twoDecimals(number: Float): String {
        return try{
            "%.2f".format(number)
        }catch (e: Exception){
            "-"
        }
    }
}