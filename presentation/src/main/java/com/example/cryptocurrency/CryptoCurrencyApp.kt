package com.example.cryptocurrency

import android.app.Application
import com.example.cryptocurrency.di.networkModule
import com.example.cryptocurrency.di.repositoryModule
import com.example.cryptocurrency.di.roomModule
import com.example.cryptocurrency.di.useCasesModule
import org.koin.android.ext.android.startKoin

class CryptoCurrencyApp: Application(){

    override fun onCreate(){
        super.onCreate()
        startKoin(
                this,
                listOf(networkModule, repositoryModule, useCasesModule, roomModule)
        )
    }

}