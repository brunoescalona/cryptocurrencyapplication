package com.example.cryptocurrency.di

import com.example.data.repository.RepositoryImplementation
import com.example.data.source.local.Dao
import com.example.data.source.remote.ApiService
import com.example.domain.repository.Repository
import org.koin.dsl.module.module

fun provideRepository(api: ApiService, dao: Dao): Repository {
    return RepositoryImplementation(api, dao)
}

val repositoryModule = module {
    single { provideRepository(get(), get()) }
}