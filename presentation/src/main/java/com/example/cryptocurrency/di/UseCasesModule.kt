package com.example.cryptocurrency.di

import com.example.domain.repository.Repository
import com.example.domain.usecases.GetTickerDataPerSymbolUseCase
import org.koin.dsl.module.module

fun provideGetTickerDataPerSymbolUseCase(repository: Repository): GetTickerDataPerSymbolUseCase {
    return GetTickerDataPerSymbolUseCase(repository)
}

val useCasesModule = module {
    single { provideGetTickerDataPerSymbolUseCase(get()) }
}