package com.example.cryptocurrency.di

import android.arch.persistence.room.Room
import com.example.cryptocurrency.CryptoCurrencyApp
import com.example.data.source.local.Dao
import com.example.data.source.local.DataBase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module

fun provideDao(app: CryptoCurrencyApp): Dao = Room
        .databaseBuilder(app, DataBase::class.java, "crypto-currency.db")
        .fallbackToDestructiveMigration()
        .build()
        .dao()

val roomModule = module  {
    single { provideDao(androidApplication() as CryptoCurrencyApp) }
}