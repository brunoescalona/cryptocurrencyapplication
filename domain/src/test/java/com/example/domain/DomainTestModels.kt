package com.example.domain

import com.example.domain.model.CryptoCurrency

fun createCryptoCurrencyTest(): CryptoCurrency {
    return CryptoCurrency(
            6499.45f,
            6500.03f,
            2.46f,
            29.34f,
            290.81f,
            154.14f,
            "2018-10-19 17:58:51"
    )
}