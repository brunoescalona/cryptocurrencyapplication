package com.example.domain.usescases

import com.example.domain.createCryptoCurrencyTest
import com.example.domain.repository.Repository
import com.example.domain.usecases.GetTickerDataPerSymbolUseCase
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test

class GetTickerDataPerSymbolUseCaseTest {

    private lateinit var getTickerDataPerSymbolUseCase: GetTickerDataPerSymbolUseCase

    private val mockRepository = mock<Repository>()

    private val symbolSet = "global"
    private val symbol = "BTCEUR"
    private val cryptoCurrencyTest = createCryptoCurrencyTest()

    private val throwable = Throwable()

    @Before
    fun setUp() {
        getTickerDataPerSymbolUseCase = GetTickerDataPerSymbolUseCase(mockRepository)
    }

    @Test
    fun executeGetTickerDataPerSymbolUseCaseWithSuccess() {
        whenever(mockRepository.getTickerDataPerSymbol(symbolSet, symbol)).thenReturn(Flowable.just(cryptoCurrencyTest))

        val testObserver = getTickerDataPerSymbolUseCase.execute(symbolSet, symbol).test()

        verify(mockRepository).getTickerDataPerSymbol(symbolSet, symbol)
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(1)
        testObserver.assertValue(cryptoCurrencyTest)
    }

    @Test
    fun executeGetTickerDataPerSymbolUseCaseWithError() {
        whenever(mockRepository.getTickerDataPerSymbol(symbolSet, symbol)).thenReturn(Flowable.error(throwable))

        val testObserver = getTickerDataPerSymbolUseCase.execute(symbolSet, symbol).test()

        verify(mockRepository).getTickerDataPerSymbol(symbolSet, symbol)
        testObserver.assertNoValues()
        testObserver.assertNotComplete()
        testObserver.assertError(throwable)
        testObserver.assertValueCount(0)
    }
}