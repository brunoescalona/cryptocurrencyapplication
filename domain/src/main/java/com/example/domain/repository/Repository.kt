package com.example.domain.repository

import com.example.domain.model.CryptoCurrency
import io.reactivex.Flowable

interface Repository {
    fun getTickerDataPerSymbol(symbolSet: String, symbol: String) : Flowable<CryptoCurrency>
}