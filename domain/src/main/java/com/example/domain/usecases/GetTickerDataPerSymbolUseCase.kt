package com.example.domain.usecases

import com.example.domain.model.CryptoCurrency
import com.example.domain.repository.Repository
import io.reactivex.Flowable

class GetTickerDataPerSymbolUseCase(val repository: Repository) {
    fun execute(symbolSet: String, symbol: String) : Flowable<CryptoCurrency>{
        return repository.getTickerDataPerSymbol(symbolSet, symbol)
    }
}