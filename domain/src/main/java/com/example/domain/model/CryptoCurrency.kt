package com.example.domain.model

data class CryptoCurrency(
        val ask: Float,
        val bid: Float,
        val changesHour: Float,
        val changesDay: Float,
        val changesWeek: Float,
        val changesMonth: Float,
        val updated: String
)