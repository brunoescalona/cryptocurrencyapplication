# Crypto currency example application

Example application created for a [stocard](https://stocardapp.com/en/au) remote case.

## Introduction
The application will be developed with Clean Architecture, with three main modules:

 - **Presentation** (All the android library dependencies and focus on the views and navigations)
 - **Data** (Repository patter to fetch the data from the server or take it from local storage)
 - **Domain** (Uses cases and domain models)

## Data module
The data module follow the Repository pattern.

The data module contains the `TickerData` model to parse the JSON response below. For more information see the [documentation](https://apiv2.bitcoinaverage.com/#ticker-data-per-symbol).

```json
{
    "ask": 6499.45,
    "bid": 6500.03,
    "last": 6499.28,
    "high": 6509.45,
    "low": 6420.32,
    "open": {
        "hour": 6496.81,
        "day": 6469.93,
        "week": 6208.47,
        "month": 6345.14,
        "month_3": 7389.53,
        "month_6": 8219.44,
        "year": 5652.65
    },
    "averages": {
        "day": 6457.44,
        "week": 6443.94,
        "month": 6440.5
    },
    "volume": 47849.51015994,
    "changes": {
        "percent": {
            "hour": 0.04,
            "day": 0.45,
            "week": 4.68,
            "month": 2.43,
            "month_3": -12.05,
            "month_6": -20.93,
            "year": 14.98
        },
        "price": {
            "hour": 2.46,
            "day": 29.34,
            "week": 290.81,
            "month": 154.14,
            "month_3": -890.25,
            "month_6": -1720.17,
            "year": 846.63
        }
    },
    "volume_percent": 81.44,
    "timestamp": 1539971931,
    "display_timestamp": "2018-10-19 17:58:51"
}
```

To store data locally and improve the UX the service data obtained is stored into the [Room](https://developer.android.com/topic/libraries/architecture/room) data base locally.

## Domain module
The domain module contains the uses cases of the application

## Presentation
The MVVM patter is used for the presentation layer of the application.

To store and manage UI-related data in a lifecycle conscious way, the [Android ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) is used

To navigate between components is used the [Android Navigation Component](https://developer.android.com/topic/libraries/architecture/navigation/)

## Tests
To run the unit test for each module, just create a junit run configuration for each module and run the test with that configuration.

## Credits
Application developed by [**Bruno Escalona Ruiz**](https://stackoverflow.com/cv/brunoescalona)