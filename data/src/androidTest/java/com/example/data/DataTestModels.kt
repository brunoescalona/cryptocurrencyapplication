package com.example.data

import com.example.data.model.Averages
import com.example.data.model.Changes
import com.example.data.model.PeriodData
import com.example.data.model.TickerData
import com.example.domain.model.CryptoCurrency

fun createTickerData(): TickerData {
    val periodData = PeriodData(
            2.46f,
            29.34f,
            290.81f,
            154.14f,
            -890.25f,
            -1720.17f,
            846.63f
    )
    val averages = Averages(
            6457.44f,
            6443.94f,
            6440.5f
    )
    val changes = Changes(
            periodData,
            periodData
    )

    return TickerData(
            "BTCEUR",
            6499.45f,
            6500.03f,
            6499.28f,
            6509.45f,
            6420.32f,
            periodData,
            averages,
            47849.51015994f,
            changes,
            81.44f,
            1539971931,
            "2018-10-19 17:58:51"
    )
}

fun createCryptoCurrencyTest(): CryptoCurrency{
    return CryptoCurrency(
            6499.45f,
            6500.03f,
            2.46f,
            29.34f,
            290.81f,
            154.14f,
            "2018-10-19 17:58:51"
    )
}
