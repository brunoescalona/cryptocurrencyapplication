package com.example.data.source.local

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import com.example.data.createTickerData
import org.junit.*

class DataBaseTest {

    private lateinit var database: DataBase
    @JvmField @Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    @Throws
    fun createDb() {
        val context = InstrumentationRegistry.getTargetContext()
        database = Room
                .inMemoryDatabaseBuilder(context, DataBase::class.java)
                .allowMainThreadQueries()
                .build()
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun insertAndGetTickerData() {
        val tickerData = createTickerData()
        database.dao().insertTickerData(tickerData)

        val testSubscriber = database.dao().getTickerData(tickerData.id).test()

        testSubscriber.assertValue{it == tickerData}
        testSubscriber.assertNoErrors()
    }

}