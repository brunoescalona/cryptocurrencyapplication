package com.example.data.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.example.domain.model.CryptoCurrency
import com.google.gson.annotations.SerializedName

/**
 * Response model for the Ticker Data (Per Symbol) service
 * @see <a href="https://apiv2.bitcoinaverage.com/#ticker-data-per-symbol">ticker data</a>
 */

@Entity(tableName = "ticker_data")
data class TickerData(
        @PrimaryKey var id: String,
        val ask: Float,
        val bid: Float,
        val last: Float,
        val high: Float,
        val low: Float,
        @Embedded(prefix = "open_") val open: PeriodData,
        @Embedded(prefix = "averages_") val averages: Averages,
        val volume: Float,
        @Embedded(prefix = "changes_") val changes: Changes,
        @ColumnInfo(name="volume_percent") @SerializedName("volume_percent") val volumePercent: Float,
        val timestamp: Int,
        @ColumnInfo(name="display_timestamp") @SerializedName("display_timestamp") val displayTimestamp: String
)

data class Averages(
        val day: Float,
        val week: Float,
        val month: Float
)

data class Changes(
        @Embedded(prefix = "percent_") val percent: PeriodData,
        @Embedded(prefix = "price_") val price: PeriodData
)

data class PeriodData(
        val hour: Float,
        val day: Float,
        val week: Float,
        val month: Float,
        @SerializedName("month_3") val threeMonths: Float,
        @SerializedName("month_6") val sixMonths: Float,
        val year: Float
)

object TickerDataMapper {
    fun mapDataToDomain(tickerData: TickerData): CryptoCurrency {
        val (_, ask, bid) = tickerData
        val (changeHour, changesDay, changesWeek, changesMonth) = tickerData.changes.percent
        return CryptoCurrency(ask, bid, changeHour, changesDay, changesWeek, changesMonth, tickerData.displayTimestamp)
    }
}