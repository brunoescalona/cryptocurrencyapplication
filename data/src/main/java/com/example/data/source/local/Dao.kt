package com.example.data.source.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.data.model.TickerData
import io.reactivex.Flowable

@Dao
interface Dao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTickerData(tickerData: TickerData)

    @Query("SELECT * FROM ticker_data WHERE id = :id")
    fun getTickerData(id: String): Flowable<TickerData>
}