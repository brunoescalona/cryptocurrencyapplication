package com.example.data.source.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.data.model.TickerData

@Database(entities = [TickerData::class], version = 1, exportSchema = false)
abstract class DataBase: RoomDatabase() {
    abstract fun dao(): Dao
}