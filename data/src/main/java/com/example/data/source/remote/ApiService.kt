package com.example.data.source.remote

import com.example.data.model.TickerData
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("indices/{symbol_set}/ticker/{symbol}")
    fun getTickerDataBySymbol(@Path("symbol_set") symbolSet: String, @Path("symbol") symbol: String): Flowable<TickerData>
}
