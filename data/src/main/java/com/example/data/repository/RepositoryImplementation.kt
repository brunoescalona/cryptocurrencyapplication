package com.example.data.repository

import com.example.data.model.TickerData
import com.example.data.model.TickerDataMapper
import com.example.data.source.local.Dao
import com.example.data.source.remote.ApiService
import com.example.domain.model.CryptoCurrency
import com.example.domain.repository.Repository
import io.reactivex.Flowable

class RepositoryImplementation(private val api: ApiService, private val dao: Dao) : Repository {

    override fun getTickerDataPerSymbol(symbolSet: String, symbol: String): Flowable<CryptoCurrency> {
        val remote: Flowable<TickerData> = api
                .getTickerDataBySymbol(symbolSet, symbol)
                .map {
                    it.id = symbol
                    dao.insertTickerData(it)
                    return@map it
                }
        val local: Flowable<TickerData> = dao.getTickerData(symbol)

        return Flowable.merge(local, remote)
                .map { TickerDataMapper.mapDataToDomain(it) }
    }
}