package com.example.data.source.remote

import com.example.data.createTickerData
import junit.framework.Assert.assertEquals
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiServiceTest {

    lateinit var mockServer : MockWebServer
    lateinit var apiService : ApiService

    private val symbolSet = "global"
    private val symbol = "BTCEUR"
    private val tickerData = createTickerData()

    @Before
    @Throws
    fun setUp() {
        mockServer = MockWebServer()
        mockServer.start()

        val okHttpClient = OkHttpClient
                .Builder()
                .build()

        val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(mockServer.url(""))
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    @Throws
    fun tearDown() {
        mockServer.shutdown()
    }

    @Test
    fun getTickerDataBySymbolTestWithNotFoundError() {
        val mockResponse = MockResponse()
                .setResponseCode(404)
        mockServer.enqueue(mockResponse)

        val testObserver = apiService.getTickerDataBySymbol(symbolSet, symbol).test()
        testObserver.awaitTerminalEvent(1, TimeUnit.SECONDS)

        testObserver.assertNoValues()
        assertEquals(1, testObserver.errorCount())
        assertEquals("/indices/$symbolSet/ticker/$symbol", mockServer.takeRequest().path)
    }

    @Test
    fun getTickerDataBySymbolTestWithSuccess() {
        val mockResponse = MockResponse()
                .setResponseCode(200)
                .setBody(getJson("ticker_data.json"))
        mockServer.enqueue(mockResponse)

        val testObserver = apiService.getTickerDataBySymbol(symbolSet, symbol).test()
        testObserver.awaitTerminalEvent(1, TimeUnit.SECONDS)

        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
        testObserver.assertValue(tickerData)
        testObserver.assertComplete()
        Assert.assertEquals("/indices/$symbolSet/ticker/$symbol", mockServer.takeRequest().path)
    }


    private fun getJson(path : String) : String {
        val aux = this.javaClass.classLoader
        val file = aux.getResourceAsStream(path)
        return String(file.readBytes())
    }
}