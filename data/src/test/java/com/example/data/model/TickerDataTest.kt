package com.example.data.model

import com.example.data.createCryptoCurrencyTest
import com.example.data.createTickerData
import junit.framework.Assert.assertEquals
import org.junit.Test

class TickerDataTest {

    @Test
    fun mapTickerDataEntityToDomainModel() {
        val tickerDataTest = createTickerData()
        val cryptoCurrencyTest = createCryptoCurrencyTest()
        val cryptoCurrencyTestMapped = TickerDataMapper.mapDataToDomain(tickerDataTest)

        assertEquals(cryptoCurrencyTest,cryptoCurrencyTestMapped)
    }
}