package com.example.data.repository

import com.example.data.createTickerData
import com.example.data.model.TickerDataMapper
import com.example.data.source.local.Dao
import com.example.data.source.remote.ApiService
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test

class RepositoryImplementationTest {

    private lateinit var repository: RepositoryImplementation

    private val mockApiService = mock<ApiService>()
    private val mockDao = mock<Dao>()

    private val symbolSet = "global"
    private val symbol = "BTCEUR"
    private val tickerData = createTickerData()

    private val throwable = Throwable()

    @Before
    @Throws
    fun setUp() {
        repository = RepositoryImplementation(mockApiService, mockDao)
    }

    @Test
    fun getTickerDataPerSymbolReturnSuccess(){
        whenever(mockApiService.getTickerDataBySymbol(symbolSet, symbol)).thenReturn(Flowable.just(tickerData))
        whenever(mockDao.getTickerData(symbol)).thenReturn(Flowable.just(tickerData))

        val testObserver = repository.getTickerDataPerSymbol(symbolSet, symbol).test()

        verify(mockApiService).getTickerDataBySymbol(symbolSet, symbol)
        verify(mockDao).getTickerData(symbol)
        testObserver.assertNoErrors()
        testObserver.assertValueAt(0, TickerDataMapper.mapDataToDomain(tickerData))
        testObserver.assertValueAt(1, TickerDataMapper.mapDataToDomain(tickerData))
        testObserver.assertValueCount(2)
        testObserver.assertComplete()
    }

    @Test
    fun getTickerDataPerSymbolReturnError(){
        whenever(mockApiService.getTickerDataBySymbol(symbolSet, symbol)).thenReturn(Flowable.error(throwable))
        whenever(mockDao.getTickerData(symbol)).thenReturn(Flowable.error(throwable))

        val testObserver = repository.getTickerDataPerSymbol(symbolSet, symbol).test()

        verify(mockApiService).getTickerDataBySymbol(symbolSet, symbol)
        testObserver.assertError(throwable)
        testObserver.assertNoValues()
        testObserver.assertNotComplete()
    }
}